package com.garrett.demo.sys.service;

import com.garrett.demo.sys.dao.UserDao;
import com.garrett.demo.sys.dao.UserDataBaseDemo;
import com.garrett.demo.sys.entity.Account;
import com.garrett.demo.sys.entity.SimsStudent;
import cn.smalltool.core.utils.IdcardUtil;

import java.util.Arrays;
public class AccountServiceImp extends AbstractAccountService {

    @Override
    public boolean verifyPassword(String pw1, String pw2) {
        if (pw1.equals(pw2)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verifyStuId(String Id) {
        if (Id!=null) {
            try {
                Integer.valueOf(Id);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }


    public boolean verifyIdCard(String idCard) {
        return IdcardUtil.isValidCard(idCard);
    }
    @Override
    public SimsStudent checkLogin(String account, String password) {
        userDao=new UserDataBaseDemo();
        Account l=userDao.getUserByAccount(account);
        if(null!=l&&l.getPassword().equals(password)){
            return userDao.getStudentById(l.getAccount());
        }
        return null;
    }
    private UserDao userDao;


    @Override
    public void createAccount(Object... obj) {
    }
    private SimsStudent[] students=new SimsStudent[3];
    private Account[] accounts=new Account[3];

    public Account[] getAllAccounts(){
        return accounts;}
    public SimsStudent[] getAllStudents(){
        return students;
    }
    public SimsStudent getStudentById(String studentId) {
        for (int i = 0; i < students.length; i++) {
            if (studentId.equals(students[i].getStudentID())){
                return students[i];
            }
        }
        return null;
    }
    public void modify(Object... objs) {

    }
    public  void deleteAccount(String id) {
        for (int i = 0; i < accounts.length; i++) {
            if (id.equals(accounts[i].getId())){
                accounts[i]=null;
                students[i]=null;
                System.out.println("删除成功！");
            }
        }
    }
    public String toString() {
        return "AccountServiceImp{" +
                "students=" + Arrays.toString(students) +
                '}';
    }
}

