package com.garrett.demo.sys.service;

import com.garrett.demo.sys.entity.SimsStudent;
import com.garrett.demo.sys.entity.Account;


public interface AccountService {
    SimsStudent checkLogin(String account, String password);
    void createAccount(Object ...obj);
    Account[] getAllAccounts();
    SimsStudent[] getAllStudents();

    SimsStudent getStudentById(String studentId);
    void modify(Object... obj);
    void deleteAccount(String id);
}
