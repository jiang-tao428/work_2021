package com.garrett.demo.sys.service;

public interface OtherLogicMethod {
    boolean verifyPassword(String pw1,String pw2);
    boolean verifyIdCard(String idCard);
    boolean verifyStuId(String id);
}