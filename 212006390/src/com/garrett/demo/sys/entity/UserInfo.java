package com.garrett.demo.sys.entity;

public class UserInfo {
    private String id;
    private String stuId;
    private String realName;
    private String birthDate;
    private String phoneNumber;
    private String gender;

    public UserInfo() {
    }

    public UserInfo(String id, String stuId, String realName) {
        this.id = id;
        this.stuId = stuId;
        this.realName = realName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStuID() {
        return stuId;
    }

    public void setStuID(String stuId) {
        this.stuId = stuId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }
}
