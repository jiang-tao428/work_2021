package com.garrett.demo.sys.entity;

import java.io.Serializable;
import java.util.Date;

public class SimsStudent implements Serializable {
    private String domicilePlaceAddress;
    private String domicilePlaceCity;
    private String domicilePlaceProvince;
    private String email;
    private String engName;
    private Date entryDate;
    private String gender;
    private Integer height;
    private String hobby;
    private String idCardNo;
    private String intro;
    private String mobilePhone;
    private String nation;
    private String political;
    private String presentAddress;
    private String studentId;
    private String studentName;
    private Integer weight;

    public SimsStudent() {

    }

    public String getDomicilePlaceAddress() {
        return domicilePlaceAddress;
    }

    public void setDomicilePlaceAddress(String domicilePlaceAddress) {
        this.domicilePlaceAddress = domicilePlaceAddress;
    }

    public String getDomicilePlaceCity() {
        return domicilePlaceCity;
    }

    public void setDomicilePlaceCity(String domicilePlaceCity) {
        this.domicilePlaceCity = domicilePlaceCity;
    }

    public String getDomicilePlaceProvince() {
        return domicilePlaceProvince;
    }

    public void setDomicilePlaceProvince(String domicilePlaceProvince) {
        this.domicilePlaceProvince = domicilePlaceProvince;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEngName() {
        return engName;
    }

    public void setEngName(String engName) {
        this.engName = engName;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getPolitical() {
        return political;
    }

    public void setPolitical(String political) {
        this.political = political;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getStudentID() {
        return studentId;
    }

    public void setStudentID(String studentID) {
        this.studentId = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }


    public String toString() {
        return "学生信息:" +
                "电子邮箱='" + email + '\'' +
                ", 性别='" + gender + '\'' +
                ", 身份证号='" + idCardNo + '\'' +
                ", 手机号='" + mobilePhone + '\'' +
                ", 学生ID='" + studentId + '\'' +
                ", 学生姓名='" + studentName + '\'';
    }

    public SimsStudent(String studentId, String studentName, String idCardNo, String mobilePhone, String gender, String email) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.idCardNo = idCardNo;
        this.mobilePhone = mobilePhone;
        this.gender = gender;
        this.email = email;
    }
}

