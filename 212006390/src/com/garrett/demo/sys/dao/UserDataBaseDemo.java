package com.garrett.demo.sys.dao;

import com.garrett.demo.sys.entity.Account;
import com.garrett.demo.sys.entity.SimsStudent;

import java.util.Arrays;
import java.util.Scanner;

public class UserDataBaseDemo implements UserDao{

    private SimsStudent[] students=new SimsStudent[3];
    private Account[]accounts=new Account[3];

    public UserDataBaseDemo() {
    }


    public int addAccount(Account account){
        for (int i = 0; i<accounts.length; i++) {
            if(accounts[i]==null){
                accounts[i]=account;
                return 1;
            }
        }
        return 0;
    }
    public int createUser(SimsStudent userInfo){
        for (int i = 0; i <students.length ; i++) {
            if(students[i]==null){
                students[i]=userInfo;
                return 1;
            }
        }
        return 0;
    }
    public SimsStudent getStudentById(java.lang.String stuId){
        for (int i = 0; i < students.length; ++i) {
            if(stuId.equals(students[i].getStudentID()))
            {
                return students[i];
            }
            else {
                continue;
            }
        }
        return null;
    }
    public Account getUserByAccount(java.lang.String account){
        for (int i = 0; i < accounts.length; ++i) {
            if(account.equals(accounts[i].getAccount())){
                return accounts[i];
            }
        }
        return null;
    }
    public int delAccountById(java.lang.String id){
        for (int i = 0; i < accounts.length; ++i) {
            if(id.equals(accounts[i].getId())){
                accounts[i]=null;
                return 1;
            }
        }
        return 0;
    }


    public int modifyAccount(Account account){
        for (int i = 0; i < accounts.length; ++i) {
            if (accounts[i].getId() != null) {
                if (account.getId().equals(accounts[i].getId())) {
                    Scanner shuru = new Scanner(System.in);
                    java.lang.String choice = shuru.nextLine();
                    if (choice.equals("1")) {
                        System.out.println("请输入新的学号：");
                        java.lang.String xuehao = shuru.nextLine();
                        accounts[i].setAccount(xuehao);
                        System.out.println("已成功修改学号为" + accounts[i].getAccount());
                        break;
                    } else if (choice.equals("2")) {
                        System.out.println("请输入新的密码：");
                        java.lang.String mima = shuru.nextLine();
                        accounts[i].setPassword(mima);
                        System.out.println("已成功修改密码为：" + accounts[i].getPassword());
                        break;
                    } else {
                        System.out.println("输入有误，请重新输入或未查询到对应账户!");
                        modifyAccount(account);
                    }

                    return 1;
                }
            }
        }
        return 0;
    }
    public int delStudentInfo(String stuId){
        for (int i = 0; i <students.length; ++i) {
            if(stuId.equals(students[i].getStudentID())){
                students[i]=null;
                return 1;
            }
        }
        return 0;
    }
    public int modifyStudentInfo(SimsStudent student) {
        for (int i = 0; i < students.length; ++i) {
            if (students[i].getStudentID() != null) {
                if (student.getStudentID().equals(students[i].getStudentID())) {
                    Scanner shuru = new Scanner(System.in);
                    String xz = shuru.nextLine();
                    if (xz.equals("1")) {
                        System.out.print("请输入新的电子邮箱：");
                        java.lang.String email = shuru.nextLine();
                        students[i].setEmail(email);
                        System.out.println("已经成功修改电子邮箱为" + students[i].getEmail());
                        tishi(student);
                    } else if (xz.equals("2")) {
                        if (students[i].getGender().equals("男")) {
                            students[i].setGender("女");
                        } else {
                            students[i].setGender("男");
                        }
                        System.out.println("已成功修改性别为" + students[i].getGender());
                        tishi(student);
                    } else if (xz.equals("3")) {
                        System.out.print("请输入新的电话号码：");
                        java.lang.String mobilePhone = shuru.nextLine();
                        students[i].setMobilePhone(mobilePhone);
                        System.out.println("已经成功修改电话号码为" + students[i].getMobilePhone());
                        tishi(student);
                    } else if (xz.equals("4")) {
                        System.out.print("请输入新的学生姓名：");
                        java.lang.String studentName = shuru.nextLine();
                        students[i].setStudentName(studentName);
                        System.out.println("已经成功修改学生姓名为" + students[i].getStudentName());
                        tishi(student);
                    } else if (xz.equals("5")) {
                        System.out.print("请输入新的身份证号码：");
                        java.lang.String idCardNo = shuru.nextLine();
                        students[i].setIdCardNo(idCardNo);
                        System.out.println("已经成功修改身份证号码为" + students[i].getIdCardNo());
                        tishi(student);
                    } else if (xz.equals("0")) {
                        System.out.println("结束！");
                        break;
                    }
                    return 1;
                }
            }
        }
        return 0;
    }

    @Override
    public SimsStudent[] getAllStudent() {
        return students;
    }

    @Override
    public java.lang.String toString() {
        return "用户数据:" +
                "学生信息" + Arrays.toString(students) +
                ", 账户数据" + Arrays.toString(accounts) +
                '。';
    }
    public void tishi(SimsStudent student){
        Scanner shuru=new Scanner(System.in);
        System.out.println("输入1继续修改，0结束");
        java.lang.String n = shuru.nextLine();
        if (n.equals("1")) {
            System.out.println("请输入要修改的选项对应序号！");
            modifyStudentInfo(student);
        } else if (n.equals("0")) {
            System.out.println("结束修改！");
        } else {
            System.out.println("您输入的有误，请重新输入！");
            modifyStudentInfo(student);
        }
    }
}