package com.garrett.demo.sys.dao;

import com.garrett.demo.sys.entity.Account;
import com.garrett.demo.sys.entity.SimsStudent;
import com.garrett.demo.sys.entity.UserInfo;

public interface UserDao {
    int addAccount(Account account);
    int createUser(SimsStudent userInfo);
    int delAccountById(String id);
    int delStudentInfo(String stuId);
    SimsStudent[] getAllStudent();
    SimsStudent getStudentById(String stuId);
    Account getUserByAccount(String account);
    int modifyAccount(Account account);
    int modifyStudentInfo(SimsStudent student);
}
