package com.garrett.demo.sys.test;

import cn.smalltool.captcha.generator.RandomGenerator;
import com.garrett.demo.sys.dao.UserDataBaseDemo;
import com.garrett.demo.sys.entity.Account;
import com.garrett.demo.sys.entity.SimsStudent;
import com.garrett.demo.sys.service.AccountService;
import com.garrett.demo.sys.service.AccountServiceImp;

import java.util.Arrays;
import java.util.Scanner;

public class ApplicationTest {
    public static void main(String[] args) {
        UserDataBaseDemo jt=new UserDataBaseDemo();
        commandTemplate();
        request(jt);
    }

    static void loginResMessage(SimsStudent user) {
        if (user != null) {
            System.out.println("登录成功！！！");
            System.out.println("欢迎登录系统，" + user.getStudentName());
        } else {
            System.out.println("登录失败！！！");
            System.out.println("提示：账户或密码错误.");
        }
    }


    public static void commandTemplate() {
        System.out.println("===================================");
        System.out.println("|                                 |");
        System.out.println("|            极简登录系统           ｜");
        System.out.println("|                                 |");
        System.out.println("===================================");

    }

    static UserDataBaseDemo login(UserDataBaseDemo jt){
        Scanner scanner = new Scanner(System.in);
        System.out.print("用户名:");
        String account = scanner.nextLine();
        System.out.print("密码：");
        String password = scanner.nextLine();
        codeCheck();
        SimsStudent userInfo=jt.getStudentById(account);
        loginResMessage(userInfo);
        return jt;
    }

    static UserDataBaseDemo register(UserDataBaseDemo jt){
        Scanner sc = new Scanner(System.in);
        SimsStudent sm=new SimsStudent();
        AccountServiceImp xt=new AccountServiceImp();
        System.out.print("请输入学号:");
        String StudentID=sc.nextLine();
        System.out.print("请输入密码:");
        String pw1=sc.nextLine();
        System.out.print("请再次输入密码：");
        String pw2=sc.nextLine();
        System.out.print("请输入手机号:");
        String MobilePhone=sc.nextLine();
        System.out.print("请输入性别:");
        String Gender=sc.nextLine();
        System.out.print("请输入身份证号:");
        String IdCardNo=sc.nextLine();
        if(xt.verifyPassword(pw1,pw2)){
            sm.setStudentID(StudentID);
            sm.setStudentName("蒋涛");
            sm.setIdCardNo(IdCardNo);
            sm.setGender(Gender);
            sm.setMobilePhone(MobilePhone);
            Account n=new Account(StudentID,StudentID,pw1);
            jt.addAccount(n);
            jt.createUser(sm);
            AccountService ac=new AccountServiceImp();
            ac.createAccount(sm);
            System.out.println("正在注册新生...");
            System.out.println("注册完成！");
        }
        else {
            System.out.println("输入的密码不一致,请重试！");
        }
        return jt;
    }
    static UserDataBaseDemo getAll(UserDataBaseDemo jt) {
        SimsStudent[] as = jt.getAllStudent();
        for (int i = 0; i < as.length; i++) {
            if(as[i]!=null) {
                System.out.println(Arrays.toString(new SimsStudent[]{as[i]}));
                System.out.println();
            }

        }
        if(as[0]==null) {
            System.out.println("当前无用户信息！");
        }
        return jt;

    }
    static UserDataBaseDemo forgot(UserDataBaseDemo jt) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入需要查询的用户名：");
        String ac = input.nextLine();
        if (jt.getUserByAccount(ac) != null) {
            Account n;
            n = jt.getUserByAccount(ac);
            System.out.println("该用户的密码为：" + n.getPassword());
        } else {
            System.out.println("未查询到对应的用户名！");
        }
        return jt;

    }
    static UserDataBaseDemo DeleteUserInfo(UserDataBaseDemo jt) {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入您所要删除的账号数据的学号");
        String id = sc.nextLine();
        if (jt.delAccountById(id) == 1 && jt.delStudentInfo(id) == 1) {
            System.out.println("删除成功！");
        } else {
            System.out.println("未查询到对应学号");
        }
        return jt;
    }
    static void request(UserDataBaseDemo jt){
        while (true) {
            System.out.println();
            System.out.println("1.学号登录");
            System.out.println("2.新生注册");
            System.out.println("3.找回密码");
            System.out.println("4.查看所有用户账户及学生信息");
            System.out.println("5.删除用户数据");
            System.out.println();
            System.out.print("请输入程序指令：");
            Scanner sc = new Scanner(System.in);
            int choice= sc.nextInt();
            if(choice==1){
                login(jt);
                continue;
            }
            else if(choice==2){
                register(jt);
                continue;
            }
            else if(choice==3){
                forgot(jt);
                continue;
            }
            else if(choice==4){
                getAll(jt);
                continue;
            }
            else if(choice==5){
                DeleteUserInfo(jt);
                continue;
            }
            else {
                break;
            }
        }
    }

    static void codeCheck() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            RandomGenerator randomGenerator = new RandomGenerator(4);
            String randomStr = randomGenerator.generate();
            System.out.println("\n请输入验证码，以区分您不是机器人");
            System.out.println("=========================");
            System.out.println("|  验证码  |    " + randomStr + "     |");
            System.out.println("=========================");
            String userCode = sc.next().trim();
            if (randomGenerator.verify(randomStr, userCode)) {
                return;
            } else {
                System.out.println("验证码错误！");
                System.out.println("正在重置验证码...");
            }
        }
    }
}
