package cn.smalltool.captcha.generator;

import cn.smalltool.core.utils.RandomUtil;
import cn.smalltool.core.utils.StrUtil;

public class RandomGenerator extends AbstractGenerator{

    public RandomGenerator(int count) {
        super(count);
    }

    public RandomGenerator(String baseStr, int length) {
        super(baseStr, length);
    }

    public String generate() {
        return RandomUtil.randomString(this.baseStr, this.length);
    }
    public boolean verify(String code, String userInputCode) {
        return StrUtil.isNotBlank(userInputCode) && StrUtil.equalsIgnoreCase(code, userInputCode);
    }
}