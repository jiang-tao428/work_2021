package cn.smalltool.captcha.generator;


public interface CodeGenerator {

    String generate();

    boolean verify(String var1, String var2);
}