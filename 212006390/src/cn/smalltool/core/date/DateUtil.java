package cn.smalltool.core.date;

import cn.smalltool.core.utils.CalendarUtil;

import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil extends CalendarUtil {

    public DateUtil() {
    }

    public static DateTime date() {
        return new DateTime();
    }

    public static int thisYear() {
        return year(date());
    }

    public static int year(Date date) {
        return DateTime.of(date).year();
    }
    public static boolean isLeapYear(int year) {
        return (new GregorianCalendar()).isLeapYear(year);
    }

    public static DateTime parse(CharSequence dateStr, String format) {
        return new DateTime(dateStr, format);
    }
}