package cn.smalltool.core.lang;

import java.util.regex.Pattern;
public class PatternPool {
    public static final Pattern NUMBERS = Pattern.compile("\\d+");
    public static final Pattern BIRTHDAY = Pattern.compile("^(\\d{2,4})([/\\-.年]?)(\\d{1,2})([/\\-.月]?)(\\d{1,2})日?$");
    public static final Pattern MOBILE = Pattern.compile("(?:0|86|\\+86)?1[3-9]\\d{9}");

}