package cn.smalltool.core.utils;

public class ObjectUtil {

    public static <T> T defaultIfNull(T object, T defaultValue) {
        return null != object ? object : defaultValue;
    }
}