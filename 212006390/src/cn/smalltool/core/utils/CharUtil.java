package cn.smalltool.core.utils;

public class CharUtil {

    public static boolean isBlankChar(char c) {
        return isBlankChar((int)c);
    }

    public static boolean isBlankChar(int c){
        // 空字符判断以及不同编码格式下的无意义字符判断
        return Character.isWhitespace(c) || Character.isSpaceChar(c) || c == 65279 || c == 8234;
    }
}