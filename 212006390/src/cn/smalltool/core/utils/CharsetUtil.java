package cn.smalltool.core.utils;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.charset.UnsupportedCharsetException;

public class CharsetUtil {
    public static final String UTF_8 = "UTF-8";
    public static final Charset CHARSET_ISO_8859_1;
    public static final Charset CHARSET_UTF_8;
    public static final Charset CHARSET_GBK;

    static {
        CHARSET_ISO_8859_1 = StandardCharsets.ISO_8859_1;
        CHARSET_UTF_8 = StandardCharsets.UTF_8;
        Charset _CHARSET_GBK = null;

        try {
            _CHARSET_GBK = Charset.forName("GBK");
        } catch (UnsupportedCharsetException var2) {
        }

        CHARSET_GBK = _CHARSET_GBK;
    }

}